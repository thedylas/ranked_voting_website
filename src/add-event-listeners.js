window.addEventListener('load', function () {
    $('form').keypress(function (event) {
        return event.keyCode !== 13;
    });

    document.getElementById("voter-name").addEventListener("keypress", function (event) {
        if (event.key === "Enter") {
            add_voter();
        }
    });

    document.getElementById("add-voter-button").addEventListener("click", add_voter);
    document.getElementById("add-voter-button").addEventListener("keypress", function (event) {
        if (event.key === "Enter") {
            event.preventDefault();
            document.getElementById("add-voter-button").click();
        }
    });

    document.getElementById("movie-search").addEventListener("keypress", function (event) {
        if (event.key === "Enter") {
            event.preventDefault();
            document.getElementById("search-movie-button").click();
        }
    });
    document.getElementById("search-movie-button").addEventListener("click", function () {
        get_movie_details(true);
        document.getElementById("search-results").style.display = "block";

        document.getElementById("poster-slide0").classList.add("active");
        document.getElementById("poster-slide1").classList.add("active");
        document.getElementById("poster-slide2").classList.add("active");

        document.getElementById("details-slide0").classList.remove("active");
        document.getElementById("details-slide1").classList.remove("active");
        document.getElementById("details-slide2").classList.remove("active");

    });
    document.getElementById("search-movie-button").addEventListener("keypress", function (event) {
        if (event.key === "Enter") {
            event.preventDefault();
            document.getElementById("search-movie-button").click();
        }
    });

    document.getElementById("begin-button").addEventListener("click", begin_voting);
    document.getElementById("begin-button").addEventListener("keypress", function (event) {
        if (event.key === "Enter") {
            event.preventDefault();
            document.getElementById("begin-button").click();
        }
    });
});

function add_event_listeners_for_searched_movies() {
    movie_details.forEach(function (element, index) {
        document.getElementById("movie-button" + index).addEventListener("click", function () {
            add_searched_movie_to_vote_on(index);
        });
        document.getElementById("movie-button" + index).addEventListener("keypress", function (event) {
            if (event.key === "Enter") {
                event.preventDefault();
                document.getElementById("movie-button").click();
            }
        });
    });
}

function add_event_listeners_for_voting() {
    for (let i = 0; i < get_remaining_movie_options().length; i++) {
        document.getElementById("vote-button" + i).addEventListener("click", function () {
            vote(document.getElementById("vote-button" + i));
            remove_votes_remaining();
            add_votes_remaining();
        });
        document.getElementById("vote-button" + i).addEventListener("keypress", function (event) {
            if (event.key === "Enter") {
                event.preventDefault();
                document.getElementById("add-voter-button").click();
            }
        });
    }
}