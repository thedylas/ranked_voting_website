voters = [];
movies_to_vote_on = [];
let winner;
let tie = false;
let tied_movie = 0;
let selected_voter_index;
let current_movie_details = 0;

class voter {
    constructor(voter_name, number_of_votes_remaining) {
        this.voter_name = voter_name;
        this.number_of_votes_remaining = number_of_votes_remaining;
        this.movies_voted_on = [];
    }
}

function get_voter_name_from_html() {
    return document.getElementById("voter-name").value;
}

function add_voter() {
    let voter_name = get_voter_name_from_html();

    if (value_is_empty(voter_name)) {
        return false;
    } else {
        let new_voter;
        new_voter = new voter(voter_name, 6);

        let get_voter = document.getElementById("voter-table").insertRow(voters.length + 2);
        let insert_voter = get_voter.insertCell(0);
        insert_voter.innerHTML = new_voter.voter_name;
        document.getElementById("current-voters").innerHTML += get_random_avatar();
        voters.push(new_voter);
        clear_field("voter-name");
    }

    if (voting_can_begin()) {
        make_voting_button_accessible();
    }
    return true;
}

function add_searched_movie_to_vote_on(index) {
    current_movie_details = movie_details[index];
    add_movie(current_movie_details.Title);
    remove_movie_that_was_added(document.getElementById("title" + index).parentNode);
}

function remove_movie_that_was_added(table) {
    let index = table.offsetParent.id.slice(-1);

    document.getElementById("carouselExampleIndicators" + index).style.display = "none";

    let poster_table = document.getElementById("searched-movie-poster" + index);
    while (poster_table.hasChildNodes()) {
        poster_table.firstChild.remove()
    }
    let details_table = document.getElementById("searched-movie-details" + index);
    while (details_table.hasChildNodes()) {
        details_table.firstChild.remove()
    }

    if (document.getElementById("carouselExampleIndicators0").style.display === "none"
        && document.getElementById("carouselExampleIndicators1").style.display === "none"
        && document.getElementById("carouselExampleIndicators2").style.display === "none") {
        document.getElementById("search-results").style.display = "none";
    }
}

function value_is_empty(field) {
    return field === "";
}

function clear_field(element_id) {
    document.getElementById(element_id).value = "";
}

function get_random_avatar() {
    let random_avatar;
    let avatar_options = [" <i class=\"fas fa-bong\"></i> ", " <i class=\"fas fa-crown\"></i> ", " <i class=\"fas fa-hat-cowboy-side\"></i> ", " <i class=\"fas fa-hard-hat\"></i> ", " <i class=\"fas fa-hat-cowboy\"></i> ", " <i class=\"fas fa-hat-wizard\"></i> "];

    random_avatar = avatar_options[(Math.floor(Math.random() * 6))];

    return random_avatar;
}

function make_voting_button_accessible() {
    if (voting_can_begin()) {
        document.getElementById("begin-button").style.display = "block";
    }
}

function fill_voter_dropdown() {
    let select = document.getElementById("voter-selection");

    for (let i = 0; i < voters.length; i++) {
        let voter = voters[i].voter_name;
        let element = document.createElement("button");
        element.textContent = voter;
        element.value = voter;
        element.type = "button";
        element.classList.add("dropdown-item");
        element.id = "voter-selection" + i;
        select.appendChild(element);
    }

    add_voter_change_event_listeners();
}

function change_dropdown_text() {
    document.getElementById("dropdown-menu").innerText = get_current_voter().voter_name;
}

function hide_pre_vote_section() {
    document.getElementById("voter-panel").style.display = "none";
    document.getElementById("movie-panel").style.display = "none";
    document.getElementById("begin-button").style.display = "none";
    document.getElementById("movie-search-panel").style.display = "none";
}

function show_voting_section() {
    document.getElementById("voting-section").style.display = "block";
}

function get_remaining_movie_options() {
    let remaining_movies = [];
    for (let i = 0; i < movies_to_vote_on.length; i++) {
        if (get_current_voter().movies_voted_on.includes(movies_to_vote_on[i])) {
        } else {
            remaining_movies.push(movies_to_vote_on[i]);
        }
    }
    return remaining_movies;
}

function fill_table_with_movies() {
    let remaining_movie_options = get_remaining_movie_options();

    for (let i = 0; i < remaining_movie_options.length; i++) {
        let movie = document.getElementById("voting-table").insertRow((i));
        let insert_movie = movie.insertCell(0);
        let insert_icon = movie.insertCell(1);
        let insert_button = movie.insertCell(2);
        insert_icon.innerHTML = remaining_movie_options[i].movie_icon;
        insert_movie.innerHTML = remaining_movie_options[i].movie_name;
        insert_button.innerHTML = '<button' + ' class="vote-button btn" id="vote-button' + i + '" class="btn" class="vote-button" type="button">Vote</button>';
    }
    add_event_listeners_for_voting();
}

function remove_movies_from_table() {
    let selected_table = document.getElementById("voting-table");
    while (selected_table.hasChildNodes()) {
        selected_table.removeChild(selected_table.firstChild);
    }
}

function add_votes_remaining() {
    document.getElementById("number-of-votes-remaining").textContent += get_current_voter().number_of_votes_remaining;
}

function remove_votes_remaining() {
    document.getElementById("number-of-votes-remaining").textContent = document.getElementById("number-of-votes-remaining").textContent.slice(0, -1);
}

function hide_voting_section() {
    document.getElementById("voting-section").style.display = "none";
}