let searched_movies = [];
let movie_details = [];

class movie_to_vote_on {
    constructor(movie_name) {
        this.movie_name = movie_name;
        this.number_of_votes = 0;
        this.movie_icon = '<i class="fas fa-gem"></i>';
    }
}

function get_movie_icon(movie) {
    let rating = movie.imdbRating;

    if (rating >= 8.5) {
        return '<i class="fas fa-palette"></i>'
    } else if (rating >= 7.5 && rating < 8.5) {
        return '<i class="fas fa-gem"></i>'
    } else {
        return '<i class="fas fa-car-crash"></i>'
    }
}

function add_movie(name) {
    let new_movie_to_vote_on = new movie_to_vote_on(name);

    let get_movie = document.getElementById("movie-table").insertRow(-1);
    let insert_movie = get_movie.insertCell(0);

    new_movie_to_vote_on.movie_icon = get_movie_icon(current_movie_details);

    insert_movie.innerHTML = new_movie_to_vote_on.movie_name + " " + new_movie_to_vote_on.movie_icon;
    movies_to_vote_on.push(new_movie_to_vote_on);

    if (voting_can_begin()) {
        make_voting_button_accessible();
    }
    document.getElementById("movie-list").style.display = "block";
}

function construct_search_url() {
    let url = 'https://www.omdbapi.com/?apikey=f73c3139&type=movie&s=';
    url += document.getElementById("movie-search").value;

    return url;
}

function search_movies() {
    searched_movies = [];
    movie_details = [];

    let url = construct_search_url();

    return fetch(url)
        .then((response) => {
            return response.json();
        })
        .then((myJson) => {
            try {
                searched_movies.push(myJson.Search[0]);
            } catch (exception) {
                clear_movie_search_table();
                document.getElementById("no-movies").innerText = "No movies were found";
                document.getElementById("search-results").style.display = "none";
            }
            try {
                searched_movies.push(myJson.Search[1]);
                searched_movies.push(myJson.Search[2]);
            } catch (exception) {
                if (exception instanceof TypeError) {
                    console.log("Less than 3 movies were returned")
                }
            }
        });
}

function get_movie_details() {
    let error = false;

    function get_movies() {
        return Promise.all([search_movies()])
    }

    get_movies()
        .then(() => {
            searched_movies.forEach(function (element, index) {
                try {
                    document.getElementById("carouselExampleIndicators" + index).style.display = "block";

                    let id = element.imdbID;
                    let url = "https://www.omdbapi.com/?apikey=f73c3139&i=" + id;

                    fetch(url)
                        .then((response) => {
                            return response.json();
                        })
                        .then((myJson) => {
                            movie_details.push(myJson);
                            current_movie_details = movie_details[0];
                            print_movie_info();
                        })
                } catch (exception) {
                    if (exception instanceof TypeError) {
                        error = true;
                        console.log("Less than 3 movies were returned");
                        document.getElementById("carouselExampleIndicators" + index).style.display = "none";
                    }
                }
            });
        });
    return error;
}

function print_movie_info() {
    clear_movie_search_table();
    clear_field("movie-search");

    movie_details.forEach(function (element, index) {
        let poster_row = document.getElementById("searched-movie-poster" + index).insertRow(-1);
        let movie_row = document.getElementById("searched-movie-details" + index).insertRow(-1);
        let genre_row = document.getElementById("searched-movie-details" + index).insertRow(-1);
        let plot_row = document.getElementById("searched-movie-details" + index).insertRow(-1);
        let add_button_row = document.getElementById("searched-movie-details" + index).insertRow(-1);

        let insert_poster = poster_row.insertCell(0);
        insert_poster.innerHTML = '<img src=\"' + element.Poster + '" alt="' + element.Title + '">';

        let insert_title = movie_row.insertCell(0);
        insert_title.innerHTML = element.Title;
        insert_title.id = ("title" + index);

        let insert_year = movie_row.insertCell(1);
        insert_year.innerHTML = "(" + element.Year + ")";
        let insert_imdb_rating = movie_row.insertCell(2);
        insert_imdb_rating.innerHTML = element.imdbRating + "/10";
        let insert_runtime = movie_row.insertCell(3);
        insert_runtime.innerHTML = element.Runtime;

        let insert_genre = genre_row.insertCell(0);
        genre_row.firstChild.colSpan = 4;
        insert_genre.classList.add("movie-info");
        insert_genre.innerHTML = element.Genre;

        let insert_plot = plot_row.insertCell(0);
        plot_row.firstChild.colSpan = 4;
        insert_plot.innerHTML = element.Plot;
        insert_plot.classList.add("plot-row");

        let insert_button = add_button_row.insertCell(0);
        insert_button.innerHTML = '<button' + ' id="movie-button' + index + '" class="btn" type="button">Add Movie</button>';
        add_button_row.firstChild.colSpan = 4;

    });
    add_event_listeners_for_searched_movies();
}

function clear_movie_search_table() {
    document.getElementById("no-movies").innerText = "";

    for (let i = 0; i < 3; i++) {
        let poster_table = document.getElementById("searched-movie-poster" + i);
        while (poster_table.hasChildNodes()) {
            poster_table.firstChild.remove()
        }
        let details_table = document.getElementById("searched-movie-details" + i);
        while (details_table.hasChildNodes()) {
            details_table.firstChild.remove()
        }
    }
}