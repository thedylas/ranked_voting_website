function begin_voting() {
    if (voting_can_begin()) {
        hide_pre_vote_section();
        show_voting_section();
        fill_voter_dropdown();
    }
}

function voting_can_begin() {
    if (voters.length > 1 && movies_to_vote_on.length > 2) {
        return true;
    }
}

function add_voter_change_event_listeners() {
    voters.forEach(function (element, index) {
        document.getElementById("voter-selection" + index).addEventListener("click", function () {
            change_selected_voter_index(index);
            change_dropdown_text();
            remove_votes_remaining();
            add_votes_remaining();
            remove_movies_from_table();
            fill_table_with_movies();
        });
    });
}

function change_selected_voter_index(index) {
    selected_voter_index = index;
    return voters[selected_voter_index];
}

function get_current_voter() {
    return voters[selected_voter_index];
}

function vote(vote_button) {
    if (get_current_voter().number_of_votes_remaining > 0) {
        let movie_name = vote_button.parentNode.parentNode.firstChild.firstChild.nodeValue;
        let movie = movies_to_vote_on.find(object => object.movie_name === movie_name);

        get_current_voter().movies_voted_on.push(movie);
        if (get_current_voter().number_of_votes_remaining === 6) {
            get_current_voter().number_of_votes_remaining -= 3;
            movie.number_of_votes += 3;
        } else if (get_current_voter().number_of_votes_remaining === 3) {
            get_current_voter().number_of_votes_remaining -= 2;
            movie.number_of_votes += 2;
        } else {
            get_current_voter().number_of_votes_remaining -= 1;
            movie.number_of_votes += 1;
        }
    }
    remove_votes_remaining();
    add_votes_remaining();
    remove_movies_from_table();
    fill_table_with_movies();
    check_for_voting_completion();

    if (get_current_voter().number_of_votes_remaining === 0) {
        document.getElementById("voter-selection" + selected_voter_index).classList.add("disabled");
    }
}

function check_for_voting_completion() {
    let number_of_completed_voters = 0;

    for (let i = 0; i < voters.length; i++) {
        if (voters[i].number_of_votes_remaining === 0) {
            number_of_completed_voters++;
        }
    }

    if (number_of_completed_voters === voters.length) {
        hide_voting_section();
        display_results_section();
        calculate_winner();
        display_winner()
    }
}

function calculate_winner() {
    let max_number_of_votes = 0;
    for (let i = 0; i < movies_to_vote_on.length; i++) {
        if (movies_to_vote_on[i].number_of_votes > max_number_of_votes) {
            winner = movies_to_vote_on[i];
            max_number_of_votes = winner.number_of_votes;
            tie = false;
        } else if (movies_to_vote_on[i].number_of_votes === max_number_of_votes) {
            tie = true;
            tied_movie = movies_to_vote_on[i];
        }
    }
}

function display_winner() {
    if (tie === false) {
        document.getElementById("winner-text").innerText += "And the winner is...\n";
        document.getElementById("winner").textContent += winner.movie_name + "!!!";
    } else if (tie === true) {
        document.getElementById("winner-text").innerText += "It was a tie...\n";
        document.getElementById("winner").textContent += winner.movie_name + " and " + tied_movie.movie_name;
        document.getElementById("tie-text").innerText += "are the winners"
    }
}

function display_detailed_results() {
    for (let i = 0; i < movies_to_vote_on.length; i++) {
        let movie = document.getElementById("detailed-results-table").insertRow(-1);
        let insert_movie = movie.insertCell(0);
        insert_movie.innerHTML = movies_to_vote_on[i].movie_name;

        let insert_movie_votes = movie.insertCell(1);
        insert_movie_votes.innerHTML = movies_to_vote_on[i].number_of_votes;
    }
}

function display_results_section() {
    document.getElementById("results-section").style.display = "block";

    display_detailed_results();
}